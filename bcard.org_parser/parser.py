# -*- coding: utf-8 -*-
"""
Python script to scrape data from website bcad.org "property search"
"""

import re
import csv
import logging
import argparse
from grab import Grab
from fuzzywuzzy import fuzz  # For string comparison. Returns scores.
from urlparse import urlsplit
from lxml.html import fromstring
from models import *

logging.basicConfig(
    filename='parser.log',
    # filemode='w',
    format='%(asctime)s: %(message)s',
    level=logging.INFO
)

REQ_URL = "http://www.bcad.org/ClientDB/PropertySearch.aspx?cid=1"
RESULTS_URL = "http://www.bcad.org/ClientDB/SearchResults.aspx"
PAGINATION_TEMPLATE = "?rtype=address&page="
PROPERTY_DETAIL_URL = "http://www.bcad.org/ClientDB/"

# Proxymesh credentials
PROXYMESH_SERVER = 'us.proxymesh.com:31280'
PROXYMESH_USERPWD = 'matlex:xlsqbq'

g = Grab()
g.setup(
    headers={
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'
    },
    proxy=PROXYMESH_SERVER,
    proxy_userpwd=PROXYMESH_USERPWD
)

# MySQL access credentials
DB_HOST = 'localhost'
DB_PORT = 3306
DB_USER = 'root'
DB_PASSWORD = 'kmpoifs8DW3'
DB_NAME = 'san_antonio_db'


def search_property(street_name, tax_year):
    """
    Makes a search query and returns page html source. Fill in fields into search form.
    :param tax_year: Execute property search and obtain actual records on given year.
    :param street_name: Execute property search based on provided street name.
    """
    # Form processing
    g.go(REQ_URL)
    g.doc.set_input('propertySearchOptions:searchType', 'Property Address')
    g.doc.set_input('propertySearchOptions:streetName', street_name)
    g.doc.set_input('propertySearchOptions:taxyear', str(tax_year))
    g.doc.set_input('propertySearchOptions:propertyType', 'All')
    g.doc.set_input('propertySearchOptions:recordsPerPage', '25')
    g.doc.set_input('propertySearchOptions:search', 'Search')
    g.doc.submit(submit_name='propertySearchOptions:search')
    return g.doc.body


def get_total_pages(html):
    """
    Takes html and counts total pages in pagination
    :param html:
    """
    parsed_body = fromstring(html)
    try:
        total_pages = parsed_body.xpath('//td[@class="paging"]/a')[-1].text_content()
        return int(total_pages)
    except IndexError:
        return 0


def get_urls_from_page(page_html):
    """
    Takes page with list of properties. Returns list with urls for each of them.
    :param page_html:
    """
    output = []
    parsed_body = fromstring(page_html)
    rows = parsed_body.xpath('//table[@id="propertySearchResults_resultsTable"]//tr')

    for row_num in range(1, len(rows) - 1):
        url = rows[row_num].xpath('*/a')[0].xpath('@href')[0]
        output.append(PROPERTY_DETAIL_URL + url)
    return output


def get_property_details(url):
    """
    Takes property url. Goes to it, parses its page and returns dictionary with property details.
    Important Attributes Needed (Fields in table)
        a. Property ID
        b. Type
        c. Property Use Description
        d. Legal Description
        e. Address
        f. Neighborhood
        g. Neighborhood CD
        h. Owner Name
        i. Mailing address
        j. Year of the property record in database
        :param url:
    """
    output = {}
    print "Processing URL:", url
    g.go(url)
    html = g.doc.body
    parsed_body = fromstring(html)
    try:
        property_id = parsed_body.xpath('//*[@id="propertyDetails"]//tr[2]//td[2]/text()')[0]
        output['property_id'] = int(property_id.strip())
    except (ValueError, IndexError) as p:
        print p
        exit(1)

    try:
        property_type = parsed_body.xpath('//*[@id="propertyDetails"]//tr[4]//td[2]/text()')[0]
        output['property_type'] = property_type.strip().replace("'", '"')
    except IndexError:
        pass
    try:
        property_use_descriprion = parsed_body.xpath('//*[@id="propertyDetails"]//tr[6]//td[2]/text()')[0]
        output['property_use_descriprion'] = property_use_descriprion.strip().replace("'", '"')
    except IndexError:
        pass
    try:
        property_legal_description = parsed_body.xpath('//td[@class="propertyDetailsLegalDescription"]/text()')[0]
        output['property_legal_description'] = property_legal_description.strip().replace("'", '"')
    except IndexError:
        pass
    try:
        property_address = parsed_body.xpath('//*[@id="propertyDetails"]//tr[8]//td[2]/text()')
        output['property_address_source'] = property_address

        property_address = [i.strip() for i in property_address]
        property_address = ', '.join(property_address)
        output['property_address'] = property_address.replace("'", '"')
    except IndexError:
        pass
    try:
        property_neighborhood = parsed_body.xpath('//*[@id="propertyDetails"]//tr[9]//td[2]/text()')[0]
        output['property_neighborhood'] = property_neighborhood.strip().replace("'", '"')
    except IndexError:
        pass
    try:
        property_neighborhood_cd = parsed_body.xpath('//*[@id="propertyDetails"]//tr[10]//td[2]/text()')[0]
        output['property_neighborhood_cd'] = property_neighborhood_cd.strip().replace("'", '"')
    except IndexError:
        pass
    try:
        property_owner_name = parsed_body.xpath('//*[@id="propertyDetails"]//tr[12]//td[2]/text()')[0]
        output['property_owner_name'] = property_owner_name.strip().replace("'", '"')
    except IndexError:
        pass
    try:
        property_owner_mailing_address_sc = parsed_body.xpath('//*[@id="propertyDetails"]//tr[13]//td[2]/text()')
        property_owner_mailing_address = []
        for index, i in enumerate(property_owner_mailing_address_sc):
            if index == 0:
                if not re.search('\d', i.strip()):
                    continue
            property_owner_mailing_address.append(i.strip())

        output['property_owner_mailing_address_source'] = property_owner_mailing_address
        property_owner_mailing_address = ', '.join(property_owner_mailing_address)
        output['property_owner_mailing_address'] = property_owner_mailing_address.replace("'", '"')
    except IndexError:
        pass
    # Extracting year property searched for
    try:
        pattern = re.compile(r"\d{4}$")
        property_record_on_year = parsed_body.xpath('//*[@id="propertyHeading_propertyInfo"]/text()')[0]
        property_record_on_year = re.search(pattern, property_record_on_year).group(0)
        output['property_record_on_year'] = property_record_on_year
    except AttributeError:
        print "I can't find property_record_on_year value. It is important for our databse."
        logging.warning("I can't find property_record_on_year value. It is important for our databse.")

        raw_input("Press 'enter' key for shutting down process.")
        exit(2)
    return output


def save_to_properties_table(property_details, owner_and_mailing_address_is_match):
    """
    :param owner_and_mailing_address_is_match: Used to indicate whether owner and mailing addresses are equal or not.
    :param property_details: Takes a dict with property_details.
    Parse it and create Owner and Property record in DB.
    """

    # MySQL Handling
    db.connect()
    # Obtaining Owner object
    owner_obj = Owners.get_or_create(
        # name="Wade Lewis"  # For debugging
        name=property_details['property_owner_name']
    )[0]

    property_reg_id_obj = PropertyRegIds.get_or_create(
        reg_id=property_details['property_id']
    )[0]

    # Obtaining Property object from Properties table
    property_obj, created = AllPropertyRecords.get_or_create(
        property_reg_id=property_reg_id_obj,
        # property_record_on_year="2011",  # For debugging
        property_record_on_year=property_details['property_record_on_year'],
        property_owner_name=owner_obj
    )

    property_obj.property_type = property_details.get('property_type', '')
    property_obj.property_use_descriprion = property_details.get('property_use_descriprion', '')
    property_obj.property_legal_description = property_details.get('property_legal_description', '')
    property_obj.property_address = property_details.get('property_address', '')
    property_obj.property_neighborhood = property_details.get('property_neighborhood', '')
    property_obj.property_neighborhood_cd = property_details.get('property_neighborhood_cd', '')
    property_obj.property_owner_mailing_address = property_details.get('property_owner_mailing_address', '')
    property_obj.owner_and_mailing_address_is_match = owner_and_mailing_address_is_match
    property_obj.save()

    db.close()
    logging.info("Successfully saved in 'AllPropertyRecords' table name.")
    print "\tSuccessfully saved in 'AllPropertyRecords' table name."
    return property_obj


def save_to_absent_table(property_object):
    """
    :param property_object: it is a object which already saved into properties_tables
    """
    property_object_with_absent_owner, created = AbsentOwners.get_or_create(
        property=property_object
    )
    if created:
        # logging.info("Successfully saved to 'absent_owners' table name.")
        print("\tSuccessfully created in 'absent_owners' table name.")
    else:
        # logging.info("Successfully created to 'absent_owners' table name.")
        print("\tSuccessfully saved in 'absent_owners' table name.")


def compare_addresses_and_store_to_db(property_details):
    """
    Takes dictionary with property details.
    Compares street address with owners mailing address and then adds details to DB if needs.

    INFO FROM Wade:
    All results (every address on the street) returned from the "property search" will have their "property address"
    and "mailing address" compared.
    * If the property address and mailing address do not match the script will save all property attributes into a
      database table called "absent_owners".
    * if street name differs though if state and zip same, it needs to be placed in 'absentee'
    * if address = "1234 memory lane, san antonio texas 78213"
      and mailing address = " 5678 Testing street, san antonio texas 78213" those would be placed in absentee
    * if we have minor differenices like that we will assume them same:
      Property address: 543 RADIANCE DR, SAN ANTONIO, TX 78218
      Owners mailing address: 543 RADIANCE AVE, SAN ANTONIO, TX 78218-2646
    * The po box can be any number so as long as it contains po box it should be processed like normal.
    * You can use the number and street name for comparison - that should be enough to be 99% accurate.

    ** All results save to a database table called "properties".

    Input dictionary fields:
    'property_id'
    'property_type'
    'property_use_descriprion'
    'property_legal_description'
    'property_address'
    'property_neighborhood'
    'property_neighborhood_cd'
    'property_owner_name'
    'property_owner_mailing_address'
    :param property_details:
    """
    partial_address = property_details.get('property_address_source')  # Using address source for manipulating
    partial_mailing_address = property_details.get('property_owner_mailing_address_source')

    print partial_address
    print partial_mailing_address

    logging.info("Property address: " + ", ".join(partial_address))
    logging.info("Owner's address:  " + ", ".join(partial_mailing_address))

    scores = fuzz.token_sort_ratio(partial_address, partial_mailing_address)
    # print scores
    if scores > 50:
        # We have possible matching
        partial_address = [i.strip().strip(',') for i in partial_address[0].split() if
                           len(i.strip().strip(',')) > 1 or i.strip().strip(',').isdigit()]

        partial_mailing_address = [i.strip().strip(',') for i in partial_mailing_address[0].split() if
                                   len(i.strip().strip(',')) > 1 or i.strip().strip(',').isdigit()]

    # print partial_address
    # print partial_mailing_address
    for elem1, elem2 in zip(partial_address, partial_mailing_address):
        # print elem1 + " and " + elem2
        if elem1.isdigit() or elem2.isdigit():
            if elem1 == elem2:
                scores += 30
            else:
                scores -= 30

    # print "scores again after number comparing", scores

    # Checking scores again
    if scores > 80:
        logging.info("* MATCH *")
        print "* MATCH *"
        match = True
        # Save each parsed property to DB into properties table
        save_to_properties_table(property_details, match)
    else:
        logging.info("* NOT MATCH *")
        print "* NOT MATCH *"
        match = False
        # Save each parsed property to DB into properties table
        property_obj = save_to_properties_table(property_details, match)
        # Saving to absent_owners table
        save_to_absent_table(property_obj)

    logging.info("\n")
    print


def parser(address_name, year_of_property_record):
    initial_body_html = search_property(address_name, year_of_property_record)
    total_pages = get_total_pages(initial_body_html)

    db.connect()

    if total_pages == 0:  # If we have no next pages then check are there any results from query
        if len(get_urls_from_page(initial_body_html)) > 0:  # We have rows with property details
            # Go through each url and take propery details
            for url in get_urls_from_page(initial_body_html):
                # Check whether property_id exists in DB or not. If not, then passing query and go to next url.
                property_id = urlsplit(url).query.split("=")[-1]
                property_id = PropertyRegIds.get_or_create(reg_id=property_id)[0]
                try:
                    AllPropertyRecords.get(
                        property_reg_id=property_id,
                        property_record_on_year=year_of_property_record
                    )
                    print url
                    logging.info(url + "\t" + address_name)
                    logging.info("Property ID exists in DB. Passing URL.")
                    print "Property ID exists in DB. Passing URL."
                    continue
                except AllPropertyRecords.DoesNotExist:
                    # Get property details from it's page then pass results into compare_addresses_and_store_to_db function.
                    property_details = get_property_details(url)
                    compare_addresses_and_store_to_db(property_details)
        else:
            print "There are no search results for address name %s" % address_name

    else:  # If we have next pages in pagination.
        print "We have next pages in pagination > total pages is:", total_pages
        # Start ineration through pages. Will start from page 1
        for page_num in range(1, total_pages + 1):
            g.go(RESULTS_URL + PAGINATION_TEMPLATE + str(page_num))
            html = g.doc.body
            property_urls = get_urls_from_page(html)
            # Then start iteration through each property url and take property details
            for url in property_urls:
                # Check whether property_id exists in DB or not. If not, then passing query and go to next url.
                property_id = urlsplit(url).query.split("=")[-1]
                property_id = PropertyRegIds.get_or_create(reg_id=property_id)[0]
                try:
                    AllPropertyRecords.get(
                        property_id=property_id,
                        property_record_on_year=year_of_property_record
                    )
                    print url
                    logging.info(url + "\t" + address_name)
                    logging.info("Property ID exists in DB. Passing URL.")
                    print "Property ID exists in DB. Passing URL."
                    continue
                except AllPropertyRecords.DoesNotExist:
                    # Get property details from it's page then pass results into compare_addresses_and_store_to_db function.
                    property_details = get_property_details(url)
                    compare_addresses_and_store_to_db(property_details)
    db.close()


def compare_and_save_properties_to_db(year_from, year_to):
    len_period = (year_to - year_from) + 1
    period_years = range(year_from, year_to + 1)
    if len_period <= 1:
        return
    # query = Properties.select().order_by(Properties.property_id, Properties.property_record_on_year)

    # Another query for extracting records from AbsentOwners only
    query = (AllPropertyRecords
             .select(AllPropertyRecords, AbsentOwners)
             .join(AbsentOwners, JOIN_INNER)
             .order_by(AllPropertyRecords.property_reg_id, AllPropertyRecords.property_record_on_year))

    property_record_id = None
    property_record_year = None
    property_record_owner = None
    # For tracking how many years ownership lasts
    owned_years_track = 0

    for current_property_obj in query:
        if current_property_obj.property_record_on_year in period_years:
            print(current_property_obj.id,
                  current_property_obj.property_reg_id.reg_id,
                  current_property_obj.property_record_on_year,
                  current_property_obj.property_owner_name.name)

            if property_record_id:  # If property_record_id not None
                # "Manipulating with records"
                if current_property_obj.property_reg_id.reg_id == property_record_id and \
                                current_property_obj.property_owner_name == property_record_owner:
                    owned_years_track += 1
                    # Now we have to compare how many years ownership lasts.
                    # And if it equal target 'len_period' then save record into separate table
                    if owned_years_track >= len_period:
                        # Save a property and owner data into another table
                        # print "We have {} year ownership!!!".format(len_period)
                        # Executing string with generated table name on the fly.
                        eval('''Equity%dYears.get_or_create(
                             property=current_property_obj,
                             ownership_year_from=%d,
                             ownership_year_to=%d
                             )''' % (len_period, year_from, year_to))
                else:
                    # Reassign/Update variables with new data because owner was changed
                    property_record_id = current_property_obj.property_reg_id.reg_id
                    property_record_year = current_property_obj.property_record_on_year
                    property_record_owner = current_property_obj.property_owner_name
                    owned_years_track = 1  # reset to 1
            else:  # Assign variables with new data if they're None
                property_record_id = current_property_obj.property_reg_id.reg_id
                property_record_year = current_property_obj.property_record_on_year
                property_record_owner = current_property_obj.property_owner_name
                owned_years_track = 1
    print
    print "Comparison process with years: {0} - {1} successfully finished.\n" \
          "All data was saved into Equity{2}Years table.".format(year_from, year_to, len_period)


def main():
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("--file", "-f", help="input csv file with address names",
                            choices=['property_addresses.csv'])
    arg_parser.add_argument("--street", "-s", help="input street name")
    arg_parser.add_argument("--compare", "-c", help="input year1 year2 for comparing")
    args = arg_parser.parse_args()
    property_bcad_record_on_year = int(raw_input("Please enter the year for which you want to receive the records: "))
    if args.file:
        print "We have file:", args.file
        with open(args.file, 'rb') as input_file:
            address_reader = csv.reader(input_file)
            for row in address_reader:
                street_name = row[0]
                print "Current street name:", street_name
                if args.compare:
                    print "Processing year comparison"
                else:
                    parser(street_name, property_bcad_record_on_year)
    elif args.street:
        print "Searching street name:", args.street
        if args.compare:
            print "Processing year comparison", args.compare.split(',')
        else:
            parser(args.street, property_bcad_record_on_year)
    else:
        print "Please provide correct additional arguments to start the program.\n" \
              "Use --help to see additional information."


if __name__ == '__main__':
    compare_and_save_properties_to_db(2011, 2016)
    # main()
    # print "Sucessfully parsed.\nJob is done."
    exit(0)
    initial_body_html_dummy = search_property("antonio", 2016)
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=1149113"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=537392"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=110096"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=108959"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=133335"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=358691"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=358705"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=501160"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=103595"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=732476"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=1097452"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=365235"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=309127"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=311599"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=365236"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=309093"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=528473"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=528472"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=309094"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=507420"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=283504"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=297319"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=374676"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=374677"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=760824"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=1107438"))
    compare_addresses_and_store_to_db(get_property_details("http://www.bcad.org/ClientDB/Property.aspx?prop_id=1193627"))  # Tricky


def compare_addresses():
    """
    Debug and score comparing.
    """
    a1 = ['15047 ANTONIO DR  ', ' TX ']
    a2 = ['15047 ANTONIO DR UNIT 7', 'HELOTES, TX 78023-4663']

    b1 = ['ANTONIO DR', ' HELOTES, TX 78023']
    b2 = ['14340 ANTONIO DR', 'HELOTES, TX 78023-3945']

    print fuzz.token_sort_ratio(a1, a2)
    print fuzz.token_sort_ratio(b1, b2)
