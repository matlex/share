from peewee import *

# MySQL access credentials
DB_HOST = 'localhost'
DB_PORT = 3306
DB_USER = 'root'
DB_PASSWORD = 'kmpoifs8DW3'
DB_NAME = 'san_antonio_db'

db = MySQLDatabase('san_antonio_db', host=DB_HOST, port=DB_PORT, user=DB_USER, passwd=DB_PASSWORD)


class BaseModel(Model):
    class Meta:
        database = db


class Owners(BaseModel):
    name = CharField(max_length=255)


class PropertyRegIds(BaseModel):
    reg_id = IntegerField(unique=True)


class AllPropertyRecords(BaseModel):
    property_reg_id = ForeignKeyField(PropertyRegIds)
    property_type = CharField(max_length=255, null=True)
    property_use_descriprion = CharField(max_length=255, null=True)
    property_legal_description = CharField(max_length=255, null=True)
    property_address = CharField(max_length=255, null=True)
    property_neighborhood = CharField(max_length=255, null=True)
    property_neighborhood_cd = CharField(max_length=255, null=True)
    property_owner_name = ForeignKeyField(Owners)
    property_owner_mailing_address = CharField(max_length=255, null=True)
    property_record_on_year = IntegerField()  # For tracking at what year current property record belongs
    owner_and_mailing_address_is_match = BooleanField(default=True)


class AbsentOwners(BaseModel):
    property = ForeignKeyField(AllPropertyRecords)


class Equity6Years(BaseModel):
    property = ForeignKeyField(AllPropertyRecords)
    ownership_year_from = IntegerField()
    ownership_year_to = IntegerField()


class Equity5Years(BaseModel):
    property = ForeignKeyField(AllPropertyRecords)
    ownership_year_from = IntegerField()
    ownership_year_to = IntegerField()


class Equity4Years(BaseModel):
    property = ForeignKeyField(AllPropertyRecords)
    ownership_year_from = IntegerField()
    ownership_year_to = IntegerField()


class Equity3Years(BaseModel):
    property = ForeignKeyField(AllPropertyRecords)
    ownership_year_from = IntegerField()
    ownership_year_to = IntegerField()


class Equity2Years(BaseModel):
    property = ForeignKeyField(AllPropertyRecords)
    ownership_year_from = IntegerField()
    ownership_year_to = IntegerField()


db.connect()
# Create new tables if doesn't exist in DB
print "Processing table creating if not exist..."
db.create_tables([Owners, PropertyRegIds, AllPropertyRecords, AbsentOwners, Equity2Years, Equity3Years, Equity4Years,
                  Equity5Years, Equity6Years], True)
db.close()
