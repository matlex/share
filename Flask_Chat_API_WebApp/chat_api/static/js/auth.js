$(document).ready(function() {
    $("#auth_button").click(function() {
        var username = $("#username").val();
        var password = $("#password").val();
        $.ajax({
            url : $("#auth_form").attr("action"),
            method: "POST",
            dataType: "json",
            data : JSON.stringify({
                username: username,
                password: password
            }),
            contentType: 'application/json;charset=UTF-8',
            success : function(json) {
                console.log(json);
                if (typeof(Storage) !== "undefined") {
                    // Code for localStorage/sessionStorage.
                    // Store auth key into local storage
                    if (localStorage.getItem("authorization_token_chatapp")) {
                        // Let's refresh auth token in LS
                        localStorage.removeItem("authorization_token_chatapp");
                        localStorage.setItem("authorization_token_chatapp", json["authorization_token"]);
                        $(location).attr('href','/chat');
                    }
                    else {
                        localStorage.setItem("authorization_token_chatapp", json["authorization_token"]);
                        $(location).attr('href','/chat');
                    }
                } else {
                    // Sorry! No Web Storage support..
                    alert("Sorry! No Web Storage support. The ChatApp will not work properly.");
                }
            },
            error : function(json) {
                $('.modal-header').addClass("alert-danger");
                $('#modalWindowTitle').html(json["responseJSON"].result);
                $('.modal-body').html("<b>" + json["responseJSON"].message + "</b>");
                $('#modalWindow').modal('show');
            }
            // error : function(xhr,errmsg,err) {
            //     alert(xhr.status + ": " + xhr.responseText);
            // }
        });
        return false;
    });
});

