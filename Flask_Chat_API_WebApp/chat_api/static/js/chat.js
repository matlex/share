$(document).ready(function () {
    // Negotiate async websocket connection
    var socket = io.connect('http://' + document.domain + ':' + location.port);

    socket.on('connect', function () {
        socket.emit('client_get_messages');
    });

    socket.on('new_messages', function () {
        socket.emit('client_get_messages');
        // receive_messages();
    });

    socket.on('receive_messages', function(msg) {
        var $messages_window = $("#messageswindow");
        $messages_window.append(
            '<strong>' + msg.data['username'] + '</strong>&nbsp;<small>' + msg.data["date"] + ':' + msg.data["time"] +
            '</small><p>' + msg.data["message_text"] + '</p>'
        );
        // Scroll to Bottom after message receiving
        $messages_window.scrollTop($messages_window.prop("scrollHeight"));
    });

    // Message field handler
    $("#message_field").keydown(function (event) {
        var keypressed = event.keyCode || event.which;
        if (keypressed == 13) {
            var message_text = $("#message_field");
            send_message(message_text.val(), socket);
            message_text.val("");
        }
    });
    // Send button handler
    $("#send_message").click(function () {
        var message_text = $("#message_field");
        send_message(message_text.val(), socket);
        message_text.val("");
    })
});

function send_message(message_text, socket) {
    $.ajax({
        url : "/api/send",
        method: "POST",
        dataType: "json",
        headers: {Authorization: localStorage.getItem("authorization_token_chatapp")},
        data : JSON.stringify({
            message_text: message_text
        }),
        contentType: 'application/json;charset=UTF-8',
        success : function() {
            socket.emit('client_broadcast_message')
        },
        error : function(xhr,errmsg,err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg);
            console.log(err);
        }
    });
}


function receive_messages() {
    $.ajax({
        url : "/api/receive",
        method: "GET",
        dataType: "json",
        headers: {Authorization: localStorage.getItem("authorization_token_chatapp")},
        contentType: 'application/json;charset=UTF-8',
        success : function(json) {
            console.log(json);
        },
        error : function(xhr,errmsg,err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg);
            console.log(err);
        }
    });
}
