from flask import Flask
from flask_socketio import SocketIO
from config import *

app = Flask(__name__)
app.config['SECRET_KEY'] = SECRET_KEY
app.config['SECURITY_TOKEN_AUTHENTICATION_HEADER'] = SECURITY_TOKEN_AUTHENTICATION_HEADER
app.config['SECURITY_TOKEN_MAX_AGE'] = SECURITY_TOKEN_MAX_AGE
app.config['PERMANENT_SESSION_LIFETIME'] = PERMANENT_SESSION_LIFETIME
app.config['MONGODB_SETTINGS'] = {
    'host': DB_CONNECTION_STRING
}
socketio = SocketIO(app)

from chat_api import views
