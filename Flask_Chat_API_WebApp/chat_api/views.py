import json
import datetime
import pika
from chat_api import app, socketio
from flask import render_template, url_for, request, session, redirect, flash, g, jsonify
from werkzeug.security import generate_password_hash, check_password_hash
from flask_mongoengine import MongoEngine, DoesNotExist
from flask_security import Security, MongoEngineUserDatastore, \
    UserMixin, RoleMixin, auth_token_required, current_user
from flask_socketio import emit

db = MongoEngine(app)


class Role(db.Document, RoleMixin):
    name = db.StringField(max_length=80, unique=True)
    description = db.StringField(max_length=255)


class User(db.Document, UserMixin):
    username = db.StringField(max_length=255, unique=True)
    # email = db.StringField(max_length=255)
    password = db.StringField(max_length=255)
    active = db.BooleanField(default=True)
    confirmed_at = db.DateTimeField()
    roles = db.ListField(db.ReferenceField(Role), default=[])


# Setup Flask-Security
user_datastore = MongoEngineUserDatastore(db, User, Role)
security = Security(app, user_datastore)


#   *-* Helpers *-*

def send_message_to_rmq(message, username):
    """
    Takes username and it's text message, forms payload and publish into username's name-based RabbitMQ queue.
    :param message:
    :param username:
    :return:
    """
    # Create connection to RMQ
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='localhost'))
    # Now create channel object
    channel = connection.channel()
    # Declaring EXCHANGE with 'fanout' type
    channel.exchange_declare(exchange='chat',
                             type='fanout')
    # Declare and execute QUEUE with name=username in case if there is no queue yet.
    result = channel.queue_declare(queue=username)
    # Bind QUEUE with EXHANGE
    channel.queue_bind(exchange='chat',
                       queue=result.method.queue)  # result.method.queue is equal username
    message_payload = {
        "username": username.capitalize(),
        "date": datetime.datetime.now().strftime('%m/%d/%y'),
        "time": datetime.datetime.now().strftime('%H:%M'),
        "message_text": message
    }
    # Publish message. No need 'routing_key' for 'fanout' exhange
    channel.basic_publish(exchange='chat',
                          routing_key='',
                          body=json.dumps(message_payload),
                          properties=pika.BasicProperties(
                              delivery_mode=2  # make message persistent
                          ))
    # print("Sent %r" % message_payload)
    connection.close()


def create_rmq_queue(username):
    """
    Creates a queue in RMQ based on User's username.
    :param username: username of authorized user
    :return:
    """
    # Create connection to RMQ
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='localhost'))
    # Now create channel object
    channel = connection.channel()
    # Declaring EXCHANGE with 'fanout' type if not exist it will be created
    channel.exchange_declare(exchange='chat',
                             type='fanout')
    # Declare and execute QUEUE with name=username in case if there is no queue yet.
    result = channel.queue_declare(queue=username)
    # Bind QUEUE with EXHANGE
    channel.queue_bind(exchange='chat',
                       queue=result.method.queue)  # result.method.queue is equal username


def get_messages_from_rmq(username):
    """
    Connects to user's RMQ Queue, iterates on messages and returns list of dictionaries with message data.
    :param username:
    :return: list of dictionaries with messages data.
    """
    output_messages = []
    connection = pika.BlockingConnection()
    channel = connection.channel()
    method_frame, header_frame, body = channel.basic_get(username)
    while method_frame:
        output_messages.append(body)
        channel.basic_ack(method_frame.delivery_tag)
        method_frame, header_frame, body = channel.basic_get(username)
    return output_messages


@app.before_request
def before_request():
    """
    Check before any request is there username in session or not. If yes, declare global user for future use.
    :return:
    """
    g.user = None
    if "username" in session:
        g.user = session["username"]


#   *-* API Resources *-*

@app.route("/api/auth", methods=["POST"])
def api_auth():
    """
    User authorization endpoint. On successful authentication sets 'authorization_token' and 'username' into
    client session.
    :return: JSON with generated authorization_token
    """
    try:
        username = request.json['username']
        user_obj = User.objects.get(username=username)
        if check_password_hash(user_obj["password"], request.json["password"]):
            # Adding authorization_token to a current session. Used for api authentication.
            authorization_token = user_obj.get_auth_token()
            session['authorization_token'] = authorization_token
            # Adding username to a current session. Used for web authentication.
            session['username'] = username
            return jsonify({
                'result': 'Success',
                'authorization_token': authorization_token,
            })
        else:
            return jsonify({
                'result': 'Failed',
                'message': "Username or password doesn't match. Check provided credentials."}), 403
    except DoesNotExist:
        return jsonify({
            'result': 'Failed',
            'message': "Username or password doesn't match. Check provided credentials."}), 403


@app.route("/api/register", methods=["POST"])
def api_register():
    """
    User registration endpoint.
    :return: JSON with authorization_token
    """
    username = request.json['username']
    password = request.json["password"]
    try:
        # User exists
        User.objects.get(username=username)
        return jsonify({
            'result': 'Failed',
            'message': "Account already exists."}), 403
    except DoesNotExist:
        # User doesn't exist, so create a new user
        hashpassword = generate_password_hash(password)
        user_obj = user_datastore.create_user(username=username, password=hashpassword)
        # Adding authorization_token to a current session. Used for api authentication.
        authorization_token = user_obj.get_auth_token()
        session['authorization_token'] = authorization_token
        # Adding username to a current session. Used for web authentication.
        session['username'] = username
        return jsonify({
            'result': 'Success',
            'authorization_token': authorization_token,
        })


@app.route("/api/send", methods=["POST"])
@auth_token_required
def api_send():
    """
    Endpoint for receiving and sending to a RMQ messages from clients.
    :return:
    """
    # And here we can push message to RabbitMQ
    authorized_user = current_user.username
    message_text = request.json["message_text"]
    send_message_to_rmq(message_text, authorized_user)
    return jsonify({
        'result': 'Success'
    })


@app.route("/api/receive", methods=["GET"])
@auth_token_required
def api_receive():
    """
    Takes from RQM queue and send new chat messages to connected client.
    :return:
    """
    output_messages = []
    authorized_user = current_user.username
    for message in get_messages_from_rmq(authorized_user):
        message = {"data": json.loads(message)}
        output_messages.append(message)
    return jsonify(output_messages)


#   SocketIO Event Handlers

@socketio.on('client_get_messages')
def client_get_messages():
    # print {'data': {
    #     "name": "Matlex",
    #     "message": "hey There!",
    #     "date": "01/01/16",
    #     "time": "18:00",
    # }}
    """
    Sends out messages to connected clients.
    :return:
    """
    if 'username' in session:
        authorized_user = session["username"]
        for message in get_messages_from_rmq(authorized_user):
            message = {"data": json.loads(message)}
            emit('receive_messages', message, json=True)


@socketio.on('client_broadcast_message')
def client_broadcast_message():
    """
    Broadcast is working. All connected clients must receive messages from RMQ.
    :return:
    """
    if 'username' in session:
        emit('new_messages', broadcast=True)


#   *-* Views *-*

@app.route("/")
def index():
    if 'username' in session:
        # If user already logged in then redirect to main chat window.
        return redirect(url_for('chat'))
    else:
        return render_template('index.html')


@app.route("/chat")
def chat():
    if g.user:
        print session['authorization_token']
        username = session["username"]
        # When user enters to chat, we need to create a new rmq queue if it doesn't exist.
        create_rmq_queue(username)
        return render_template('chat_window.html', username=username.capitalize())
    else:
        return "You're not allowed enter the ChatApp without authorization.\n" \
               'Please return to <a href="/">main page</a> and login again.'


@app.route("/register", methods=["POST", "GET"])
def register_user():
    if request.method == "POST":
        user = User.objects.get(username=request.form['username'])

        if not user:
            # Lets register a new user and add it to a DB
            hashpassword = generate_password_hash(request.form['userpassword'])  # Encrypt a user password into hash
            user_datastore.create_user(username=request.form['username'], password=hashpassword)
            session['username'] = request.form['username']
            return redirect(url_for('index'))
        else:
            return "That username already exists."
    return render_template('register.html')


@app.route("/login_user", methods=["POST"])
def login_user():
    user = User.objects.get(username=request.form["username"])

    if user:
        if check_password_hash(user["password"], request.form["password"]):
            session["username"] = request.form["username"]
            return redirect(url_for('index'))
        else:
            return "Invalid username/password combination"
    return "Invalid username/password combination"


@app.route("/logout_user")
def logout_user():
    if "username" in session:
        session.pop("username", None)
        flash("Successfully logged out from ChatApp")
        return redirect(url_for('index'))
