run in terminal:
python flyingmachine_co_in_parser.py

The following directories must be created manually - size_charts, output_files

The file structure is:
* Script_Directory
  - flyingmachine_co_in_parser.py
  * size_charts(dir)
	here size_charts created automatically
  * output_files(dir)
	here output files (json and excel) created automatically
