# -*- coding: utf-8 -*-
"""
This parser starts from http://www.flyingmachine.co.in/shop-by-fit-men.html and crawls through all category pages, then
parses all product urls and get all details from each product. Also it finds Size Chart table and scrapes data from it.
Output data saves into Excel and JSON.
"""

import requests
from openpyxl import Workbook
from openpyxl.styles import Font, PatternFill, Alignment
from lxml.html import fromstring
import json
import time
from os import listdir
import logging
logging.basicConfig(filename='warning.log', level=logging.WARNING)

__author__ = 'coder@pythoncoder.su'


# Helper Functions
def get_html(url):
    return requests.get(url).content


def check_stop_list(stop_list, str_to_check):
    for word in stop_list:
        if word in str_to_check:
            return True


def find_next_page_pagination(page_html):
    """
    Find next page url through @class="next i-next". The next page appears while scrolling down onto the web-page.
    """
    parsed_body = fromstring(page_html)
    try:
        next_page_source = parsed_body.xpath('//a[@class="next i-next"]/@href')[0]
        return next_page_source
    except IndexError:
        return None


def get_all_pages():
    """
    url_to_parse_queue - temporary storing urls for parsing
    all_category_pages - for returning all found pages in category.
    """
    url_to_parse_queue = ['http://www.flyingmachine.co.in/shop-by-fit-men.html']
    all_category_pages = ['http://www.flyingmachine.co.in/shop-by-fit-men.html']
    while True:
        if len(url_to_parse_queue) > 0:
            html_to_parse = requests.get(url_to_parse_queue.pop()).text
            if find_next_page_pagination(html_to_parse):
                next_page = find_next_page_pagination(html_to_parse)
                # print next_page
                url_to_parse_queue.append(next_page)
                # Adding to our returning list
                all_category_pages.append(next_page)
        else:
            break
    return all_category_pages


def get_products_urls(category_url):
    """
    Gets all products urls from current page of category.
    """
    stop_list = ["shirt", "cargo", "sweatshirt", "trouser"]
    category_html = get_html(category_url)
    output_list = []
    parsed_body = fromstring(category_html)
    urls = parsed_body.xpath('//h3[@class="product-name"]')
    # print len(urls), urls
    for h3 in urls:
        url = h3.xpath('a/@href')[0]
        if not check_stop_list(stop_list, h3.text_content().lower()):
            output_list.append(url)
    return output_list


# Main Functions
def get_data_from_product(product_url):
    """
    Output format:
    [{Product URL, Display Name, Display Picture URL, Product Code, Price, Fit(Size+Fit Section)}, {Size Chart Dict}]
    """
    product_details_output = {}
    product_html = requests.get(product_url).content
    parsed_body = fromstring(product_html)

    # Product URL
    product_details_output["product_url"] = product_url

    # Display Name
    try:
        product_name = parsed_body.xpath('//div[@class="product-name"]/h1/text()')[0]
        product_details_output["product_name"] = product_name
    except AttributeError:
        pass

    # Display Picture URL
    try:
        product_picture_url = parsed_body.xpath('//div[@id="main_img_gal"]//a')[0].xpath('@href')[0]
        product_details_output["product_picture_url"] = product_picture_url
    except AttributeError:
        print("CANT FIND IMAGE")
        pass

    # Product Code
    try:
        product_code = parsed_body.xpath('//span[@class="sku"]/text()')[0]
        product_details_output["product_code"] = product_code
    except AttributeError:
        pass

    # Price
    try:
        product_price = parsed_body.xpath('//div[@class="product-essential row"]//div[@class="price-box"]//text()')
        product_price = ", ".join([price.strip().replace(",", "") for price in product_price if len(price.strip()) > 1])
        product_details_output["product_price"] = product_price
    except AttributeError:
        pass

    # Fit(Size+Fit Section)
    try:
        product_fit = parsed_body.xpath('//div[@id="content_tab_sizefit_tabbed"]//ul//li')[-1]
        product_fit = product_fit.text_content().strip()
        product_details_output["product_fit_and_size"] = product_fit
    except AttributeError:
        pass

    # Size Chart File
    """
    Some trick here. When click on Size Chart button, javascript(into html source code) knows which table of sizes
    to show for product.
    Therefore we find out which table will show the javascript and find that table. Then parse it and store result.
    """
    size_chart_output = {}
    script_source = parsed_body.xpath('//*[@id="product-options-wrapper"]/script')[0]
    script_source = script_source.text_content()

    for line in script_source.splitlines(True):
        if 'content' in line:
            first_quote = line.find('"')
            second_quote = line.find('"', first_quote+1)
            line = line[first_quote+1:second_quote]
            size_chart_table_name = line.split()[-1][1:]

            # Size Chart Table - Parsing the found table.
            xpath_name = '//div[@id="%s"]' % size_chart_table_name

            try:
                # If didnt find so there is no size chart data for current product.
                div = parsed_body.xpath(xpath_name)[0]
                table_rows = (div.xpath('table/tbody//tr'))
            except (IndexError, AttributeError):
                return [product_details_output, size_chart_output]

            # Gathering partial data and forming a table.

            # Table title.
            table_header = div.xpath('table/tbody//td[@class="ce8"]')[0]
            table_header = table_header.text_content().strip()
            size_chart_output["table_header"] = table_header

            # Column headers.
            column_headers = div.xpath('table/tbody//td[@class="ce15"]')
            column_headers = list((column_header.text_content().replace("(Inches)", "").strip() for column_header in column_headers))
            column_headers.insert(0, "Size")

            # Then we need to get a number of a string tr where lies column headers. So we can achieve when data lines
            # starts from.
            data_starts_from = int()
            for row_number, row in enumerate(table_rows):
                if "Flying Machine Size" in row.text_content():
                    data_starts_from = row_number

            # Now gathering a data. Starting from found earlier data_starts_from and + 1.
            # Iterating over lines and columns.
            for column_number, column_name in enumerate(column_headers):
                column_values = []
                for row_number, row in enumerate(table_rows[data_starts_from + 1:], 0):
                    column_values.append(row[column_number].text_content().strip())
                size_chart_output[column_name] = column_values

    # print(product_details_output)
    # print(size_chart_output)
    # Returns list with two items. The first - dictionary with product details, second - dict wit size chart.
    return [product_details_output, size_chart_output]


def main():
    t_start = time.localtime()
    print("Scraping started at: %02d:%02d:%02d" % (t_start[3], t_start[4], t_start[5]))

    # For Excel
    workbook = Workbook()
    excel_row = 2
    sheet = workbook.active
    sheet.title = "Men's Jeans"

    first_row_font = Font(name='Calibri', bold=True, size=12)
    first_row_alignment = Alignment(horizontal='center')

    sheet.cell(row=1, column=1).value = u'Product URL:'
    sheet.cell(row=1, column=2).value = u'Display Name:'
    sheet.cell(row=1, column=3).value = u'Display Picture URL:'
    sheet.cell(row=1, column=4).value = u'Product Code:'
    sheet.cell(row=1, column=5).value = u'Price: Actual, Discount'
    sheet.cell(row=1, column=6).value = u'Fit(Size+Fit Section):'
    sheet.cell(row=1, column=7).value = u'Size Chart:'

    sheet.column_dimensions["A"].width = 30
    sheet.column_dimensions['B'].width = 30
    sheet.column_dimensions['C'].width = 30
    sheet.column_dimensions['D'].width = 15
    sheet.column_dimensions['E'].width = 15
    sheet.column_dimensions['F'].width = 25
    sheet.column_dimensions['G'].width = 30

    for column_num in range(1, 8):
        sheet.cell(row=1, column=column_num).font = first_row_font
        sheet.cell(row=1, column=column_num).fill = PatternFill(patternType='solid', fgColor="538dd5")
        sheet.cell(row=1, column=column_num).alignment = first_row_alignment

    # For JSON
    json_table_dict = {}

    product_count = 0

    for page_url in get_all_pages():
        print("Processing page %s" % page_url)
        for url in get_products_urls(page_url):
            print("\tProcessing %s" % url)
            data = (get_data_from_product(url))
            product_details = data[0]  # Dict with product details

            product_size_chart = data[1]  # Dict with product's size_chart table data.
            if len(product_size_chart) > 0:
                # Generating a size_chart file
                size_chart_filename = product_size_chart.get("table_header")
                if size_chart_filename + ".txt" not in listdir("size_charts"):
                    f = open("size_charts/" + size_chart_filename + ".txt", "w")
                    # table head
                    f.write(size_chart_filename + "\n")
                    # table size
                    f.write("Size" + "\t" + '\t'.join(product_size_chart.get("Size")) + "\n")
                    # Inserting size chart data into file.
                    for size_data in product_size_chart.items():
                        data_header = size_data[0]
                        data_values = size_data[1]
                        if data_header != "table_header" and data_header != "Size":
                            f.write(data_header + "\t" + '\t'.join(data_values) + "\n")
                    f.close()

                # Saving to json
                json_table_dict[product_count] = {"product_data": product_details, "size_chart_data": product_size_chart}
            else:
                size_chart_filename = None
                # Saving to json
                json_table_dict[product_count] = {"product_data": product_details, "size_chart_data": "NULL"}

            # Saving to xlsx
            sheet.cell(row=excel_row, column=1).value = product_details.get("product_url", None)
            sheet.cell(row=excel_row, column=1).hyperlink = product_details.get("product_url", None)
            sheet.cell(row=excel_row, column=2).value = product_details.get("product_name", None)
            sheet.cell(row=excel_row, column=3).value = product_details.get("product_picture_url", None)
            sheet.cell(row=excel_row, column=3).hyperlink = product_details.get("product_picture_url", None)
            sheet.cell(row=excel_row, column=4).value = product_details.get("product_code", None)
            sheet.cell(row=excel_row, column=5).value = product_details.get("product_price", None)
            sheet.cell(row=excel_row, column=6).value = product_details.get("product_fit_and_size", None)
            if size_chart_filename:
                sheet.cell(row=excel_row, column=7).value = "size_chart_filename" + ".txt"
                sheet.cell(row=excel_row, column=7).hyperlink = "../size_charts/" + size_chart_filename + ".txt"
            excel_row += 1
            product_count += 1

    print("Saving spreadsheet.")
    workbook.save('output_files/flyingmachine_co_in.xlsx')
    print("Subcategory scraped successfully.")

    f = open("output_files/JSON_DB.json", "w")
    f.write(json.dumps(json_table_dict, sort_keys=True, indent=4))
    f.close()

    print("Scraping was started at: %02d:%02d:%02d" % (t_start[3], t_start[4], t_start[5]))
    t = time.localtime()
    print("Scraping finished at: %02d:%02d:%02d" % (t[3], t[4], t[5]))


if __name__ == '__main__':
    main()
